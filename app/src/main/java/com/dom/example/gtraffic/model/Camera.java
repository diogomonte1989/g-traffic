package com.dom.example.gtraffic.model;

/**
 * Created by dom on 30/10/16.
 */

/*
{
"ID": 1,
"StorageDurationMinutes": 30,
"CaptureIntervalSeconds": 60,
"Description": "Korsvägen mot Mölndalsvägen ",
"Model": "Axis 2120"
}
*/

public class Camera {

    private int id;
    private int StorageDurationMinutes;
    private int CaptureIntervalSeconds;
    private String description;

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public int getStorageDurationMinutes() {
        return StorageDurationMinutes;
    }

    public void setStorageDurationMinutes(int storageDurationMinutes) {
        StorageDurationMinutes = storageDurationMinutes;
    }

    public int getCaptureIntervalSeconds() {
        return CaptureIntervalSeconds;
    }

    public void setCaptureIntervalSeconds(int captureIntervalSeconds) {
        CaptureIntervalSeconds = captureIntervalSeconds;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        Camera c = (Camera) obj;
        if (c.getId() == this.id) {
            return true;
        }
        return false;
    }
}
